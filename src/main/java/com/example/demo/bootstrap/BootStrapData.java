package com.example.demo.bootstrap;

import com.example.demo.domain.OutsourcedPart;
import com.example.demo.domain.Part;
import com.example.demo.domain.Product;
import com.example.demo.repositories.OutsourcedPartRepository;
import com.example.demo.repositories.PartRepository;
import com.example.demo.repositories.ProductRepository;
import com.example.demo.service.OutsourcedPartService;
import com.example.demo.service.OutsourcedPartServiceImpl;
import com.example.demo.service.ProductService;
import com.example.demo.service.ProductServiceImpl;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

/**
 *
 *
 *
 *
 */
@Component
public class BootStrapData implements CommandLineRunner {

    private final PartRepository partRepository;
    private final ProductRepository productRepository;

    private final OutsourcedPartRepository outsourcedPartRepository;

    public BootStrapData(PartRepository partRepository, ProductRepository productRepository, OutsourcedPartRepository outsourcedPartRepository) {
        this.partRepository = partRepository;
        this.productRepository = productRepository;
        this.outsourcedPartRepository=outsourcedPartRepository;
    }

    @Override
    public void run(String... args) throws Exception {
        long partCount = partRepository.count();
        long productCount = productRepository.count();

        /* Example showing how to create a part:
        OutsourcedPart o= new OutsourcedPart();
        o.setCompanyName("Western Governors University");
        o.setName("out test");
        o.setInv(5);
        o.setPrice(20.0);
        o.setId(100L);
        outsourcedPartRepository.save(o);
        OutsourcedPart thePart=null;
        List<OutsourcedPart> outsourcedParts=(List<OutsourcedPart>) outsourcedPartRepository.findAll();
        for(OutsourcedPart part:outsourcedParts){
            if(part.getName().equals("out test"))thePart=part;
        }

        System.out.println(thePart.getCompanyName());
        */

        List<OutsourcedPart> outsourcedParts=(List<OutsourcedPart>) outsourcedPartRepository.findAll();
        for(OutsourcedPart part:outsourcedParts){
            System.out.println(part.getName()+" "+part.getCompanyName());
        }

        if (partCount == 0 && productCount == 0) {
            // Creating parts
            OutsourcedPart tabletop= new OutsourcedPart();
            tabletop.setCompanyName("Tabletop Company");
            tabletop.setName("Tabletop");
            tabletop.setInv(35);
            tabletop.setMinInv(15); // Minimum inventory value
            tabletop.setMaxInv(40); // Maximum inventory value
            tabletop.setPrice(400.0);
            tabletop.setId(1);

            OutsourcedPart legs= new OutsourcedPart();
            legs.setCompanyName("Leg Company");
            legs.setName("Legs");
            legs.setInv(45);
            legs.setMinInv(40); // Minimum inventory value
            legs.setMaxInv(100); // Maximum inventory value
            legs.setPrice(80.0);
            legs.setId(2);

            OutsourcedPart shelves= new OutsourcedPart();
            shelves.setCompanyName("Shelf Company");
            shelves.setName("Shelves");
            shelves.setInv(12);
            shelves.setMinInv(8); // Minimum inventory value
            shelves.setMaxInv(32); // Maximum inventory value
            shelves.setPrice(28.0);
            shelves.setId(3);

            OutsourcedPart sidePanels= new OutsourcedPart();
            sidePanels.setCompanyName("Side Panel Company");
            sidePanels.setName("Side Panels");
            sidePanels.setInv(5);
            sidePanels.setMinInv(2); // Minimum inventory value
            sidePanels.setMaxInv(10); // Maximum inventory value
            sidePanels.setPrice(40.0);
            sidePanels.setId(4);

            OutsourcedPart drawers= new OutsourcedPart();
            drawers.setCompanyName("Drawer Company");
            drawers.setName("Drawers Panels");
            drawers.setInv(15);
            drawers.setMinInv(8); // Minimum inventory value
            drawers.setMaxInv(25); // Maximum inventory value
            drawers.setPrice(60.0);
            drawers.setId(5);

            outsourcedPartRepository.save(tabletop);
            outsourcedPartRepository.save(legs);
            outsourcedPartRepository.save(shelves);
            outsourcedPartRepository.save(sidePanels);
            outsourcedPartRepository.save(drawers);

            // Creation of parts
            Product diningTable= new Product("Dining Table",300.0,8);
            Product bookshelf = new Product("Bookshelf",240.0,12);
            Product coffeeTable= new Product("Coffee Table",90.0,15);
            Product bedFrame = new Product("Bed Frame",500.0,6);
            Product desk = new Product("Desk",280.0,9);

            productRepository.save(diningTable);
            productRepository.save(bookshelf);
            productRepository.save(coffeeTable);
            productRepository.save(bedFrame);
            productRepository.save(desk);

            System.out.println("Sample inventory added.");

            // Uncomment if I need to print the added inventory
            // System.out.println("Parts: " + partRepository.findAll());
            // System.out.println("Products: " + productRepository.findAll());
        } else {
            System.out.println("Inventory already exists, skipping sample inventory addition.");
        }

            System.out.println("Started in Bootstrap");
            System.out.println("Number of Products"+productRepository.count());
            System.out.println(productRepository.findAll());
            System.out.println("Number of Parts"+partRepository.count());
            System.out.println(partRepository.findAll());

    }
}
